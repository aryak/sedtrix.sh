# This file contains example json responses i can play with.

- Multi-Line Message
```
[ { "content": { "body": "a\nb\nc\nd\ne", "msgtype": "m.text", "org.matrix.msc1767.text": "a\nb\nc\nd\ne" }, "origin_server_ts": 1644733519582, "room_id": "!iCDfEXsntzbAGCDvKw:matrix.org", "sender": "@arya:envs.net", "type": "m.room.message", "unsigned": {}, "event_id": "$UUYPjFIcM9zCM9nwA9xZXE0neipRrESEoOFEmE3M8Ts", "user_id": "@arya:envs.net" } ]
```
- Single-Line Message
```
[ { "content": { "body": "test", "msgtype": "m.text", "org.matrix.msc1767.text": "test" }, "origin_server_ts": 1644733652519, "room_id": "!iCDfEXsntzbAGCDvKw:matrix.org", "sender": "@arya:envs.net", "type": "m.room.message", "unsigned": {}, "event_id": "$M9DYldCStKqksl-zN9Ob4L5xHlz6K8UqTdJL0DLEIek", "user_id": "@arya:envs.net" } ]
```
- Single-Line Reply
```
[ { "content": { "body": "> <@arya:envs.net> test\n\nhi", "format": "org.matrix.custom.html", "formatted_body": "<mx-reply><blockquote><a href=\"https://matrix.to/#/!iCDfEXsntzbAGCDvKw:matrix.org/$M9DYldCStKqksl-zN9Ob4L5xHlz6K8UqTdJL0DLEIek?via=matrix.org&via=envs.net&via=kde.org\">In reply to</a> <a href=\"https://matrix.to/#/@arya:envs.net\">@arya:envs.net</a><br>test</blockquote></mx-reply>hi", "m.relates_to": { "m.in_reply_to": { "event_id": "$M9DYldCStKqksl-zN9Ob4L5xHlz6K8UqTdJL0DLEIek" } }, "msgtype": "m.text", "org.matrix.msc1767.message": [ { "body": "> <@arya:envs.net> test\n\nhi", "mimetype": "text/plain" }, { "body": "<mx-reply><blockquote><a href=\"https://matrix.to/#/!iCDfEXsntzbAGCDvKw:matrix.org/$M9DYldCStKqksl-zN9Ob4L5xHlz6K8UqTdJL0DLEIek?via=matrix.org&via=envs.net&via=kde.org\">In reply to</a> <a href=\"https://matrix.to/#/@arya:envs.net\">@arya:envs.net</a><br>test</blockquote></mx-reply>hi", "mimetype": "text/html" } ] }, "origin_server_ts": 1644733694551, "room_id": "!iCDfEXsntzbAGCDvKw:matrix.org", "sender": "@arya:envs.net", "type": "m.room.message", "unsigned": {}, "event_id": "$hwN2V7maD89YXluisF_4ERleJI3OJ2hs13XWugy4xlk", "user_id": "@arya:envs.net" } ]
test\n\nhi
```
- Single-Line reply to Multi-Line message
```
[ { "content": { "body": "> <@arya:envs.net> a\n> b\n> c\n> d\n> e\n\nhello", "format": "org.matrix.custom.html", "formatted_body": "<mx-reply><blockquote><a href=\"https://matrix.to/#/!iCDfEXsntzbAGCDvKw:matrix.org/$XX3BCtVyZtfHkytMigM0xdwhhYtZXRjgxSl2WDxKSkA?via=matrix.org&via=envs.net&via=kde.org\">In reply to</a> <a href=\"https://matrix.to/#/@arya:envs.net\">@arya:envs.net</a><br>a<br/>b<br/>c<br/>d<br/>e</blockquote></mx-reply>hello", "m.relates_to": { "m.in_reply_to": { "event_id": "$XX3BCtVyZtfHkytMigM0xdwhhYtZXRjgxSl2WDxKSkA" } }, "msgtype": "m.text", "org.matrix.msc1767.message": [ { "body": "> <@arya:envs.net> a\n> b\n> c\n> d\n> e\n\nhello", "mimetype": "text/plain" }, { "body": "<mx-reply><blockquote><a href=\"https://matrix.to/#/!iCDfEXsntzbAGCDvKw:matrix.org/$XX3BCtVyZtfHkytMigM0xdwhhYtZXRjgxSl2WDxKSkA?via=matrix.org&via=envs.net&via=kde.org\">In reply to</a> <a href=\"https://matrix.to/#/@arya:envs.net\">@arya:envs.net</a><br>a<br/>b<br/>c<br/>d<br/>e</blockquote></mx-reply>hello", "mimetype": "text/html" } ] }, "origin_server_ts": 1644733757713, "room_id": "!iCDfEXsntzbAGCDvKw:matrix.org", "sender": "@arya:envs.net", "type": "m.room.message", "unsigned": {}, "event_id": "$j3X9SrpCq5tYyHBq_R1XuNXkuswNoIWfmfcqux_FkN4", "user_id": "@arya:envs.net" } ]
```

