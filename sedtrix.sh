#!/bin/bash
declare -A NextSyncTokenForRooms # Matrix Sync Token
# shellcheck source=.env
source .env # Contains all required variables
TxnId() { # Random chars. Needed for sending messages. I am not using tr on /dev/urandom for speed reasons
  CharRange='1234567890abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ'
  for ((i = 0; i <= 32; i++)); do
    printf '%s' "${CharRange:$((RANDOM % ${#CharRange})):1}"
  done
}

SendMessage() {
  BaseUrl=$1
  AccessToken=$2
  RoomId=$3
  Message=${4//\"/}

  curl -s \
    -X PUT \
    -H 'Content-Type: application/json' \
    -H 'Authorization: Bearer '"$AccessToken" \
    -d '{"msgtype":"m.text", "body":"'"$Message"'"}' \
    "$BaseUrl"'/_matrix/client/r0/rooms/'"$RoomId"'/send/m.room.message/'"$(TxnId)"
}

ReadMessages() {
  BaseUrl=$1
  AccessToken=$2
  RoomId=$3
  NextSyncToken=$4
  Limit=${5:-100}

  curl -s \
    -X GET \
    -H 'Authorization: Bearer '"$AccessToken" \
    "$BaseUrl"'/_matrix/client/r0/rooms/'"$RoomId"'/messages?limit='"$Limit"'&dir=f&from='"$NextSyncToken"
}
SedPass() {
  MessageWithReplies=${1//$'\n'/\\n} # $1 but convert all the newlines to ascii \n so sed doesnt get confused
  ManipulationText=${MessageWithReplies%%\\n*} # Only get first part of MessageWithReplies.
  Regex=$(echo "${MessageWithReplies##*\\n}" | cut -d' ' -f2-) # Get second part (regex) of MessageWithReplies but delete first word (sed) just incase they try to run other commands.
  CompletedRegex=$(echo "$ManipulationText" | sed "$Regex") # Pipe the text needing to be manipulated into sed with the regex.
  Message=${CompletedRegex//$'\n'/\\n} # Message is the CompletedRegex but with newlines converted into ascii \n so curl doesnt get confused
  SendMessage "$BaseUrl" "$AccessToken" "$RoomId" "$Message"
}
Main() {
  if ! Res=$(ReadMessages "$BaseUrl" "$AccessToken" "$RoomId" "${NextSyncTokenForRooms["$RoomId"]}"); then
    exit 1
  fi


  # Get NextSyncToken
  NextSyncToken=$(printf '%s' "$Res" | jq --raw-output '.end')
  [[ $? == 0 && $NextSyncToken != 'null' ]] && NextSyncTokenForRooms["$RoomId"]=$NextSyncToken

  # Read chunks
  ChunkArr=$(printf '%s' "$Res" | jq '.chunk')
  [[ $ChunkArr == '[]' ]] && return 0

  echo "$(date) $ChunkArr" >>sedtrix.log # Log Message Json with Exact Time 
  #echo $ChunkArr
  Message=$(echo "$ChunkArr" | jq '.[] | .content.body' | tr -d '"' | sed s/\>\ \<\@.*\>\ //) # Get only content.body from json, remove "" and remove the username from message (in case its a reply)
  echo "$Message" # For Debugging purposes
  if echo "${Message##*\\n}" | grep '^sed'; then
  echo Success # For debugging purposes
  SedPass "$Message"
  fi
}

while :; do
  Main
  sleep 1
done
